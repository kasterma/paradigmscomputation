sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y emacs23 cmake g++ subversion git python python-dev openjdk-6-jdk
# mozart needs emacs
# buildig mozart needs cmake, g++, git
# for building llvm and gtest need subversion
# mozart building needs boost, boost needs python and python-dev for complete build
# mozart needs java
# tcl8.4 was also installed, but suspect only need the dev stuff
sudo apt-get install -y tcl8.4-dev tk-dev
