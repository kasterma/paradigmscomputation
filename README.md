# Notes etc for Pardigms of Computer Programming Coursera course

## Compiling mozart in a virtual machine.

I have used Vagrant with vmware on OS X Mavericks to try and compile
mozart in a vm.  First on the host get all the sources, so that on
multiple restarts don't need to get the sources again and again.  Then
start the vm and compile the dependencies, followed by (trying to)
comiling mozart.

### Current state

Current state of compiling mozart2 in virtual machine: seem to get all
dependencies installed, but then on the final make of mozart get:

    vagrant@precise64:~/resources/builds/mozart2-release$ make
    [  0%] Building CXX object vm/generator/main/CMakeFiles/generator.dir/implementations.cc.o
    In file included from /home/vagrant/resources/mozart2/vm/generator/main/implementations.cc:25:0:
    /home/vagrant/resources/mozart2/vm/generator/main/generator.hh: In function ‘T getValueParamAsIntegral(const SpecDecl*) [with T = unsigned char, SpecDecl = clang::ClassTemplateSpecializationDecl]’:
    /home/vagrant/resources/mozart2/vm/generator/main/generator.hh:134:67:   instantiated from ‘T getValueParamAsIntegral(clang::CXXRecordDecl*) [with T = unsigned char]’
    /home/vagrant/resources/mozart2/vm/generator/main/implementations.cc:258:54:   instantiated from here
    /home/vagrant/resources/mozart2/vm/generator/main/generator.hh:126:77: error: invalid initialization of non-const reference of type ‘llvm::APSInt&’ from an rvalue of type ‘llvm::APSInt’
    make[2]: *** [vm/generator/main/CMakeFiles/generator.dir/implementations.cc.o] Error 1
    make[1]: *** [vm/generator/main/CMakeFiles/generator.dir/all] Error 2
    make: *** [all] Error 2

While I was working on this, I managed to fix brew so that I could get it
on my mac with:

    brew tap eregon/mozart2
    brew install --HEAD mozart2

That takes the pressure of figuring out the compilation error.  Leaving
this for now.

Note: the Makefiles are currently largely a copy paste of what I did on the
terminal; with just those you might not get all the way to the error.